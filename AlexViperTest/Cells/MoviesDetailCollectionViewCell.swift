//
//  MoviesDetailCollectionViewCell.swift
//  AlexViperTest
//
//  Created by Alexander on 10/02/22.
//

import UIKit

class MoviesDetailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backGroundImageView: UIImageView!
    
    func configure(index: IndexPath, companies: [Production_companies]){
        let elements = companies[index.row]
        guard let url = URL(string: "https://image.tmdb.org/t/p/w200/\(elements.logo_path ?? "")") else {return}
        let data = try? Data(contentsOf: url)

        if let imageData = data {
            let image = UIImage(data: imageData)
            backGroundImageView.image = image
        }
    }
}
