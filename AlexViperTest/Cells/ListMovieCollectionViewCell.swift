//
//  ListMovieCollectionViewCell.swift
//  AlexViperTest
//
//  Created by Alexander on 08/02/22.
//

import UIKit

class ListMovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var nameMovieLabel: UILabel!
    @IBOutlet weak var dateMovieLabel: UILabel!
    @IBOutlet weak var ratingMovieLabel: UILabel!
    @IBOutlet weak var overViewMovieLabel: UILabel!
    
    func configure(index: IndexPath, results: [Results]){
        
        let elements = results[index.row]
        guard let url = URL(string: "https://image.tmdb.org/t/p/w200/\(elements.poster_path ?? "")") else {return}
        let data = try? Data(contentsOf: url)

        if let imageData = data {
            let image = UIImage(data: imageData)
            movieImageView.image = image
        }
        
        nameMovieLabel.text = elements.original_title
        dateMovieLabel.text = elements.release_date
        ratingMovieLabel.text = String(elements.vote_average ?? 0.0)
        overViewMovieLabel.text = elements.overview
    }
    
    
}
