//
//  Alerts.swift
//  AlexViperTest
//
//  Created by Alexander on 07/02/22.
//

import Foundation
import UIKit

struct Singleton {
    
    static var shared = Singleton()
    var loginResponse: LoginResponse?
    var userName: String?
    
    private init(){
        
    }
    
    func getLoginResponse ()-> LoginResponse?{
        return loginResponse
    }
    
    mutating func setLoginResponse(sessionId: LoginResponse){
        loginResponse = sessionId
    }
    
    func getUserName ()-> String?{
        return userName
    }
    
    mutating func setUserName(name: String){
        userName = name
    }
    
    
    
    
}
