//
//  LoginAccountLoginAccountPresenter.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

class LoginAccountPresenter: LoginAccountModuleInput {
    
    

    weak var view: LoginAccountViewInput!
    var interactor: LoginAccountInteractorInput!
    var router: LoginAccountRouterInput!

    func viewIsReady() {

    }
    
}

extension LoginAccountPresenter: LoginAccountInteractorOutput{
    
    func didUpdateSession(_ loginResponse: LoginResponse) {
        view.didUpdateSession(loginResponse)
    }

    func didRetrieveAccessLoginSucess(_ loginResponse: LoginResponse) {
        view.didRetrieveAccessLoginSucess(loginResponse)
    }
    
    func didRetrieveAccessLoginFailure(_ message: String) {
        view.didRetrieveAccessLoginFailure(message)
    }
    
    
    func didRetrieveTokenSuccess(_ tokenResponse: TokenResponse) {
        view.didRetrieveTokenSuccess(tokenResponse)
    }
    
    func didRetrieveTokenFailure(_ message: String) {
        view.didRetrieveTokenFailure(message)
    }
    
    
}

extension LoginAccountPresenter: LoginAccountViewOutput{
    
    func createSession(_ loginRequest: LoginRequest) {
        interactor.createSession(loginRequest)
    }
    
    func retrieveToken() {
        interactor.retrieveToken()
    }
    
    func retrieveAccessLogin(request: LoginRequest) {
        interactor.retrieveAccessLogin(request: request)
    }
    
    func didTapLogin() {
        router.openScreen()
    }
}
