//
//  LoginAccountLoginAccountViewOutput.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

protocol LoginAccountViewOutput {

    /**
        @author alexander
        Notify presenter that view is ready
    */

    func viewIsReady()
    func retrieveToken()
    func retrieveAccessLogin(request: LoginRequest)
    func didTapLogin()
    func createSession(_ loginRequest: LoginRequest)

}
