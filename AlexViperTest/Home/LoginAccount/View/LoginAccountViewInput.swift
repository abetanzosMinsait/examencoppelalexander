//
//  LoginAccountLoginAccountViewInput.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

protocol LoginAccountViewInput: AnyObject {

    /**
        @author alexander
        Setup initial state of the view
    */

    func setupInitialState()
    func didRetrieveTokenSuccess(_ tokenResponse: TokenResponse)
    func didRetrieveTokenFailure(_ message: String)
    func didRetrieveAccessLoginSucess(_ loginResponse: LoginResponse)
    func didRetrieveAccessLoginFailure(_ message: String)
    func didUpdateSession(_ loginResponse: LoginResponse)
}
