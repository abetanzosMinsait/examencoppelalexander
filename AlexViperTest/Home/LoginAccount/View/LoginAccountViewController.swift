//
//  LoginAccountLoginAccountViewController.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import UIKit

class LoginAccountViewController: UIViewController {
    
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var warningErrorLabel: UILabel!
    
    var router: LoginAccountRouter!
    var output: LoginAccountViewOutput!
    var tokenResponse: TokenResponse!
    var loginResponse: LoginResponse!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
        output.retrieveToken()
        
        
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        guard let user = userNameTextField.text, user != "" else {
            return
        }
        
        Singleton.shared.setUserName(name: user)
        
        guard let password = passwordTextField.text, password != "" else {
            return
        }
        
        let loginRequest = LoginRequest(username: user, password: password, request_token: self.tokenResponse.request_token)
        self.output.retrieveAccessLogin(request: loginRequest)
        
        
    }
    
    // MARK: LoginAccountViewInput
    func setupInitialState() {
    }
}

extension LoginAccountViewController: LoginAccountViewInput{
    
    func didUpdateSession(_ loginResponse: LoginResponse) {
        Singleton.shared.setLoginResponse(sessionId: loginResponse)
    }
    
    func didRetrieveTokenSuccess(_ tokenResponse: TokenResponse) {
        self.tokenResponse = tokenResponse
        
        debugPrint("token_response:\(tokenResponse)")
    }
    
    func didRetrieveTokenFailure(_ message: String) {
        //Singleton.shared.showMessageError(message, from: self)
    }
    
    func didRetrieveAccessLoginSucess(_ loginResponse: LoginResponse) {
        DispatchQueue.main.async {
            if self.tokenResponse.success == true{
                self.loginResponse = loginResponse
                if self.loginResponse.success == true{
                    self.output.didTapLogin()
                    let request = LoginRequest(username: "", password: "", request_token: loginResponse.request_token ?? "")
                    self.output.createSession(request)
                }else {
                    self.warningErrorLabel.text = loginResponse.status_message
                }
            }
        }
    }
    
    func didRetrieveAccessLoginFailure(_ message: String) {
        warningErrorLabel.text = message
    }
    
    
}
