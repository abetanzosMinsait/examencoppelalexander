//
//  LoginAccountLoginAccountConfigurator.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import UIKit

class LoginAccountModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? LoginAccountViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: LoginAccountViewController) {

        let router = LoginAccountRouter()
        router.viewController = viewController

        let presenter = LoginAccountPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = LoginAccountInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
