//
//  LoginAccountLoginAccountInitializer.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import UIKit

class LoginAccountModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var loginaccountViewController: LoginAccountViewController!

    override func awakeFromNib() {

        let configurator = LoginAccountModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: loginaccountViewController)
    }

}
