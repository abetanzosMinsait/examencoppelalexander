//
//  TokenResponse.swift
//  AlexExamenCoppel
//
//  Created by Alexander on 31/01/22.
//

import Foundation

struct TokenResponse: Decodable{
    
    var success: Bool
    var expires_at: String
    var request_token: String
}

struct LoginResponse: Decodable{
    
    var success: Bool?
    var expires_at: String?
    var request_token: String?
    var status_code: Int?
    var status_message: String?
    var session_id: String?
}
