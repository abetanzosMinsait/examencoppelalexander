//
//  LoginRequest.swift
//  AlexViperTest
//
//  Created by Alexander on 08/02/22.
//

import Foundation

struct LoginRequest: Decodable{
    
    var username: String
    var password: String
    var request_token: String
}
