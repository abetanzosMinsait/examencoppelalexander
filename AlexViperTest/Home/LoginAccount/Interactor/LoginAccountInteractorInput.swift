//
//  LoginAccountLoginAccountInteractorInput.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import Foundation

protocol LoginAccountInteractorInput {

    func retrieveToken()
    func retrieveAccessLogin(request: LoginRequest)
    func createSession(_ loginRequest: LoginRequest)
}
