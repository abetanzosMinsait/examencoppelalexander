//
//  LoginAccountLoginAccountInteractor.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

class LoginAccountInteractor: LoginAccountInteractorInput {
    
    weak var output: LoginAccountInteractorOutput!
    var apiRequest = APIRequest()
    
    func retrieveToken() {
        apiRequest.getToken(){ res in
            switch res{
            case .success(let tokenResponse):
                self.output.didRetrieveTokenSuccess(tokenResponse)
            case .failure(let error):
                self.output.didRetrieveTokenFailure(error.localizedDescription)
            }
        }
    }
    
    func createSession(_ loginRequest: LoginRequest) {
        apiRequest.createSession(params: loginRequest){ res in
            switch res{
            case .success(let response):
                self.output.didUpdateSession(response)
            case .failure(_):
                break;
            }
        }
    }
    
    func retrieveAccessLogin(request: LoginRequest) {
        apiRequest.retrieveSession(params: request){ res in
            switch res{
            case .success(let response):
                self.output.didRetrieveAccessLoginSucess(response)
            case .failure(let error):
                self.output.didRetrieveAccessLoginFailure(error.localizedDescription)
            }
        }
    }
    
}
