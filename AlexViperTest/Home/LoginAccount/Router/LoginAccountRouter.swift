//
//  LoginAccountLoginAccountRouter.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import UIKit

class LoginAccountRouter: LoginAccountRouterInput {
    
    weak var viewController: LoginAccountViewController!
    
    func openScreen() {
        /*
        let storyboard = UIStoryboard(name: "ListPelis", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: "ListPelisViewController") as! ListPelisViewController
        VC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(VC, animated: true)
         */
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "ListPelis", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "ListPelisViewController")
        newViewController.modalPresentationStyle = .fullScreen
        //viewController.navigationController?.pushViewController(newViewController, animated: true)
        viewController.present(newViewController, animated: true)
    }

}
