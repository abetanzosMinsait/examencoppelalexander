//
//  ListPelisListPelisInitializer.swift
//  AlexViperTest
//
//  Created by alexander on 05/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import UIKit

class ListPelisModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var listpelisViewController: ListPelisViewController!

    override func awakeFromNib() {

        let configurator = ListPelisModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: listpelisViewController)
    }

}
