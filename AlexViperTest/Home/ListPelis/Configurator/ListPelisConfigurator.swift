//
//  ListPelisListPelisConfigurator.swift
//  AlexViperTest
//
//  Created by alexander on 05/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import UIKit

class ListPelisModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? ListPelisViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: ListPelisViewController) {

        let router = ListPelisRouter()
        router.viewController = viewController

        let presenter = ListPelisPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = ListPelisInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
