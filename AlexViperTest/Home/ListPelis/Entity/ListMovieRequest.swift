//
//  ListMovieRequest.swift
//  AlexExamenCoppel
//
//  Created by Alexander on 31/01/22.
//

import Foundation

struct ListMovieRequest{
    
    var api_key: String
    var language: String?
    var page: Int?
    var region: String?
}
