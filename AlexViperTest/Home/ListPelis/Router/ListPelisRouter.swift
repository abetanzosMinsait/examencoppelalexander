//
//  ListPelisListPelisRouter.swift
//  AlexViperTest
//
//  Created by alexander on 05/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//
import UIKit

class ListPelisRouter: ListPelisRouterInput {
   
    weak var viewController: ListPelisViewController!
    
    func openDetail(idMovie: Int) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "ListPelis", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "DetailMoviesViewController") as! DetailMoviesViewController
        newViewController.modalPresentationStyle = .fullScreen
        newViewController.idMovie = idMovie
        //viewController.navigationController?.pushViewController(newViewController, animated: true)
        viewController.present(newViewController, animated: true)
    }
    
    func openProfile() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "ListPelis", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        newViewController.modalPresentationStyle = .automatic
        //viewController.navigationController?.pushViewController(newViewController, animated: true)
        viewController.present(newViewController, animated: true)
    }
    
    func dismissLogin() {
        viewController.dismiss(animated: true)
    }
    
}
