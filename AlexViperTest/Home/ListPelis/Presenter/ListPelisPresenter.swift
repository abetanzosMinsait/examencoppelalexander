//
//  ListPelisListPelisPresenter.swift
//  AlexViperTest
//
//  Created by alexander on 05/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

class ListPelisPresenter: ListPelisModuleInput {
    
    weak var view: ListPelisViewInput!
    var interactor: ListPelisInteractorInput!
    var router: ListPelisRouterInput!

    func viewIsReady() {
        
    }
    
    func updateRetrieve() {
        interactor.retrieveData()
    }
}

extension ListPelisPresenter: ListPelisViewOutput{
    func didTabToLoginView() {
        router.dismissLogin()
    }
    
    func didTabLogOut(sessionId: String) {
        interactor.logOutAction(sessionId: sessionId)
    }
    
    func didTabNavigateToProfile() {
        router.openProfile()
    }
    
    func didTabToMoreMenu() {
        view.showAlertView()
    }
    
    func didTabToDetail(idMovie: Int) {
        router.openDetail(idMovie: idMovie)
    }
}

extension ListPelisPresenter: ListPelisInteractorOutput{
    
    func retrieveLogOutSuccess(message: String) {
        view.retrieveLogOutSuccess(message: message)
    }
    
    func retrieveLogOutFailure(message: String) {
        view.retrieveLogOutFailure(message: message)
    }
    
    func retrieveSuccessString(listMovieResponse: ListMovieResponse) {
        view.retrieveSuccessString(listMovieResponse: listMovieResponse)
    }
    
    func retrieveFailure(message: String) {
        view.retrieveFailure(message: message)
    }
}
