//
//  ListPelisListPelisViewController.swift
//  AlexViperTest
//
//  Created by alexander on 05/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import UIKit

class ListPelisViewController: UIViewController, ListPelisViewInput {
    
    @IBOutlet weak var listMoviesCollectionView: UICollectionView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    var output: ListPelisViewOutput!
    var listMovieResponse: ListMovieResponse!
    var results: [Results] = []
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.updateRetrieve()
        setupConfig()
    }
    
    func setupConfig(){
        segmentedControl.setTitle("Pupular", forSegmentAt: 0)
        segmentedControl.setTitle("Top Rated", forSegmentAt: 1)
        segmentedControl.setTitle("On TV", forSegmentAt: 2)
        segmentedControl.setTitle("Airing Today", forSegmentAt: 3)
        segmentedControl.backgroundColor = .black
        
    }
    
    // MARK: ListPelisViewInput
    func setupInitialState() {
    }
    
    func retrieveLogOutSuccess(message: String) {
        
    }
    
    func retrieveLogOutFailure(message: String) {
        
    }
    
    func retrieveSuccessString(listMovieResponse: ListMovieResponse) {
        DispatchQueue.main.async {
            self.listMovieResponse = listMovieResponse
            self.results = listMovieResponse.results ?? []
            debugPrint("results:\(self.results)")
            self.listMoviesCollectionView.reloadData()
        }
    }
    
    func retrieveFailure(message: String) {
        DispatchQueue.main.async {
            //Singleton.shared.showMessageError(message, from: self)
        }
    }
    
    
    @IBAction func navigateHome(_ sender: UIButton) {
        output.didTabToMoreMenu()
    }
    
    func showAlertView() {
        
        let alert = UIAlertController(title: "", message: "What do you want to do?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "View Profile", style: .default, handler: { (_) in
            self.output.didTabNavigateToProfile()
            print("User click Approve button")
        }))
        
        alert.addAction(UIAlertAction(title: "Log out", style: .destructive, handler: { (_) in
            let sessionId = Singleton.shared.getLoginResponse()?.session_id
            self.output.didTabLogOut(sessionId: sessionId ?? "")
            self.output.didTabToLoginView()
            print("User click Delete button")
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
            print("User click Dismiss button")
        }))
        
        present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
}

extension ListPelisViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.results.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cv = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ListMovieCollectionViewCell
        cv.configure(index: indexPath, results: self.results)
        return cv
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let idMovie = results[indexPath.row].id
        self.output.didTabToDetail(idMovie: idMovie ?? 0)
        
    }
}
