//
//  ListPelisListPelisViewOutput.swift
//  AlexViperTest
//
//  Created by alexander on 05/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

protocol ListPelisViewOutput {

    func viewIsReady()
    func updateRetrieve()
    func didTabToDetail(idMovie: Int)
    func didTabToMoreMenu()
    func didTabNavigateToProfile()
    func didTabLogOut(sessionId: String)
    func didTabToLoginView()

}
