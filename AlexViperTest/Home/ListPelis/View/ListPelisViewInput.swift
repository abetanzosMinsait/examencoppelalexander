//
//  ListPelisListPelisViewInput.swift
//  AlexViperTest
//
//  Created by alexander on 05/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

protocol ListPelisViewInput: AnyObject {
    
    func setupInitialState()
    func retrieveSuccessString(listMovieResponse: ListMovieResponse)
    func retrieveFailure(message: String)
    func showAlertView()
    func retrieveLogOutSuccess(message: String)
    func retrieveLogOutFailure(message: String)
}
