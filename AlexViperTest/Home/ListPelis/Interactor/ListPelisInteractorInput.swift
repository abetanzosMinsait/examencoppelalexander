//
//  ListPelisListPelisInteractorInput.swift
//  AlexViperTest
//
//  Created by alexander on 05/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import Foundation

protocol ListPelisInteractorInput {
    
    func retrieveData()
    func logOutAction(sessionId: String)

}
