//
//  ListPelisListPelisInteractor.swift
//  AlexViperTest
//
//  Created by alexander on 05/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

class ListPelisInteractor: ListPelisInteractorInput {
    
    
    
    weak var output: ListPelisInteractorOutput!
    var apiRequest = APIRequest()
    
    func retrieveData() {
        let request = ListMovieRequest(api_key: "", language: "", page: 1, region: "")
        apiRequest.retireveListMovies(request: request){ result in
            switch result{
            case .success( let listMovieResponse):
                self.output.retrieveSuccessString(listMovieResponse: listMovieResponse)
            case .failure(let error):
                self.output.retrieveFailure(message: error.localizedDescription)
                break;
            }
        }
    }
    
    func logOutAction(sessionId: String) {
        apiRequest.logOutSession(sessionId: sessionId){ res in
            switch res{
            case .success(let response):
                if response.success ?? false{
                    self.output.retrieveLogOutSuccess(message: "Se cerró la sesión exitosamente")
                }
            case .failure(let error):
                self.output.retrieveFailure(message: error.localizedDescription)
            }
        }
    }
    
}
