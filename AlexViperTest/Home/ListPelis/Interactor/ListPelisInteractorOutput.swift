//
//  ListPelisListPelisInteractorOutput.swift
//  AlexViperTest
//
//  Created by alexander on 05/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import Foundation

protocol ListPelisInteractorOutput: AnyObject {

    func retrieveSuccessString(listMovieResponse: ListMovieResponse)
    func retrieveFailure(message: String)
    func retrieveLogOutSuccess(message: String)
    func retrieveLogOutFailure(message: String)
}
