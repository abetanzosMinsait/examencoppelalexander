//
//  ProfileProfilePresenter.swift
//  AlexViperTest
//
//  Created by alexander on 10/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

class ProfilePresenter: ProfileModuleInput, ProfileViewOutput, ProfileInteractorOutput {

    weak var view: ProfileViewInput!
    var interactor: ProfileInteractorInput!
    var router: ProfileRouterInput!

    func viewIsReady() {

    }
}
