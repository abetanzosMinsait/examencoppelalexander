//
//  ProfileProfileInitializer.swift
//  AlexViperTest
//
//  Created by alexander on 10/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import UIKit

class ProfileModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var profileViewController: ProfileViewController!

    override func awakeFromNib() {

        let configurator = ProfileModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: profileViewController)
    }

}
