//
//  ProfileProfileViewController.swift
//  AlexViperTest
//
//  Created by alexander on 10/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, ProfileViewInput {

    var output: ProfileViewOutput!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameProfileLabel: UILabel!
    

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
        let name = Singleton.shared.getUserName()
        nameProfileLabel.text = "@\(name ?? "")"
    }

    

    // MARK: ProfileViewInput
    func setupInitialState() {
    }
}
