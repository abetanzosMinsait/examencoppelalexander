//
//  ProfileProfileViewOutput.swift
//  AlexViperTest
//
//  Created by alexander on 10/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

protocol ProfileViewOutput {

    /**
        @author alexander
        Notify presenter that view is ready
    */

    func viewIsReady()
}
