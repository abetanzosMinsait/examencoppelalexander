//
//  ProfileProfileViewInput.swift
//  AlexViperTest
//
//  Created by alexander on 10/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

protocol ProfileViewInput: class {

    /**
        @author alexander
        Setup initial state of the view
    */

    func setupInitialState()
}
