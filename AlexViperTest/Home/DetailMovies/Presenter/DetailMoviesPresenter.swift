//
//  DetailMoviesDetailMoviesPresenter.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

class DetailMoviesPresenter: DetailMoviesModuleInput {

    weak var view: DetailMoviesViewInput!
    var interactor: DetailMoviesInteractorInput!
    var router: DetailMoviesRouterInput!

    func viewIsReady() {

    }
}

extension DetailMoviesPresenter: DetailMoviesViewOutput{
    
    func retrieveDetailMovie(idMovie: Int) {
        interactor.retrieveDetailMovie(idMovie: idMovie)
    }
    
    func didTabBack() {
        router.didTabBack()
    }
    
}

extension DetailMoviesPresenter: DetailMoviesInteractorOutput{
    func didRetrieveDetailMoviesSuccess(_ listMovieResponse: Results) {
        view.didUpdateDetailMoviesSuccess(listMovieResponse)
    }
    
    func didRetrieveDetailMoviesFailure(_ message: String) {
        view.didUpdateDetailMoviesFailure(message)
    }
    
}
