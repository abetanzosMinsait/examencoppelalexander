//
//  DetailMoviesDetailMoviesRouter.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

class DetailMoviesRouter: DetailMoviesRouterInput {
    
    weak var viewController: DetailMoviesViewController!
    
    func didTabBack() {
        viewController.dismiss(animated: true)
    }
    

}
