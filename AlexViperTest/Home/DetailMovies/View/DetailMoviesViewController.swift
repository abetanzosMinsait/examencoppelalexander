//
//  DetailMoviesDetailMoviesViewController.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import UIKit

class DetailMoviesViewController: UIViewController {

    @IBOutlet weak var detailMoviesCollectionView: UICollectionView!
    @IBOutlet weak var moviesImageView: UIImageView!
    @IBOutlet weak var overiewLabel: UILabel!
    @IBOutlet weak var nameMovieLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var spokenLanguajesLabel: UILabel!
    @IBOutlet weak var productionCountriesLabel: UILabel!
    
    var output: DetailMoviesViewOutput!
    var listMovieDetail: Results!
    var companies: [Production_companies] = []
    var genres: [String] = []
    var spoken_languages: [String] = []
    var production_countries: [String] = []
    var idMovie = 0

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.retrieveDetailMovie(idMovie: self.idMovie)
        debugPrint("idMovie: \(idMovie)")
    }


    // MARK: DetailMoviesViewInput
    func setupInitialState() {
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        output.didTabBack()
    }
    
}

extension DetailMoviesViewController: DetailMoviesViewInput{
    
    func didUpdateDetailMoviesSuccess(_ listMovieResponse: Results) {
        
        self.listMovieDetail = listMovieResponse
        self.companies = listMovieResponse.production_companies ?? []
        configElements(listMovieResponse: listMovieResponse)
        
        
    }
    
    func configElements(listMovieResponse: Results){
        DispatchQueue.main.async {
            self.showSpinner(onView: self.view)
            guard let url = URL(string: "https://image.tmdb.org/t/p/w500/\(self.listMovieDetail.poster_path ?? "")") else {return}
            let data = try? Data(contentsOf: url)

            if let imageData = data {
                let image = UIImage(data: imageData)
                self.moviesImageView.image = image
            }
            
            self.nameMovieLabel.text = listMovieResponse.original_title
            self.overiewLabel.text = listMovieResponse.overview
            //let joined = listMovieResponse.joined(separator: ", ")
            for elements in listMovieResponse.genres ?? [] {
                self.genres.append(elements.name ?? "")
            }
            
            for elements in listMovieResponse.spoken_languages ?? [] {
                self.spoken_languages.append(elements.english_name ?? "")
            }
            
            for elements in listMovieResponse.production_countries ?? [] {
                self.production_countries.append(elements.name ?? "")
            }
            let genres = self.genres.joined(separator: ", ")
            let spoken_languages = self.spoken_languages.joined(separator: ", ")
            let production_countries = self.production_countries.joined(separator: ", ")
            self.genreLabel.text = "Genres: \(genres)"
            self.spokenLanguajesLabel.text = "Spoken Lenguajes: \(spoken_languages)"
            self.productionCountriesLabel.text = "Production countries: \(production_countries)"
            self.detailMoviesCollectionView.reloadData()
            self.removeSpinner()
        }
    }
    
    func didUpdateDetailMoviesFailure(_ message: String) {
        //Singleton.shared.showMessageError(message, from: self)
    }
}

extension DetailMoviesViewController: UICollectionViewDelegate, UICollectionViewDataSource{
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.companies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cv = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MoviesDetailCollectionViewCell
        cv.configure(index: indexPath, companies: self.companies)
        return cv
    }
    
    
}
