//
//  DetailMoviesDetailMoviesViewOutput.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

protocol DetailMoviesViewOutput {

    /**
        @author alexander
        Notify presenter that view is ready
    */

    func viewIsReady()
    func didTabBack()
    func retrieveDetailMovie(idMovie: Int)
}
