//
//  DetailMoviesDetailMoviesViewInput.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

protocol DetailMoviesViewInput: class {

    /**
        @author alexander
        Setup initial state of the view
    */

    func setupInitialState()
    func didUpdateDetailMoviesSuccess(_ listMovieResponse: Results)
    func didUpdateDetailMoviesFailure(_ message: String)
}
