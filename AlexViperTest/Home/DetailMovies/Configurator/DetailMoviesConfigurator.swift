//
//  DetailMoviesDetailMoviesConfigurator.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import UIKit

class DetailMoviesModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? DetailMoviesViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: DetailMoviesViewController) {

        let router = DetailMoviesRouter()
        router.viewController = viewController

        let presenter = DetailMoviesPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = DetailMoviesInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
