//
//  DetailMoviesDetailMoviesInitializer.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import UIKit

class DetailMoviesModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var detailmoviesViewController: DetailMoviesViewController!

    override func awakeFromNib() {

        let configurator = DetailMoviesModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: detailmoviesViewController)
    }

}
