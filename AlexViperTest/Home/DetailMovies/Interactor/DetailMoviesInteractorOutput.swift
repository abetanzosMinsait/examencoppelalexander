//
//  DetailMoviesDetailMoviesInteractorOutput.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import Foundation

protocol DetailMoviesInteractorOutput: AnyObject {

    func didRetrieveDetailMoviesSuccess(_ listMovieResponse: Results)
    func didRetrieveDetailMoviesFailure(_ message: String)
}
