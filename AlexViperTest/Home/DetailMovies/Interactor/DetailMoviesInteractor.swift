//
//  DetailMoviesDetailMoviesInteractor.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

class DetailMoviesInteractor: DetailMoviesInteractorInput {
    
    weak var output: DetailMoviesInteractorOutput!
    var apiRequest = APIRequest()

    func retrieveDetailMovie(idMovie: Int) {
        apiRequest.retrieveDetailMovies(idMovie: idMovie){ res in
            switch res{
            case .success(let response):
                self.output.didRetrieveDetailMoviesSuccess(response)
            case .failure(let error):
                self.output.didRetrieveDetailMoviesFailure(error.localizedDescription)
            }
        }
    }
}
