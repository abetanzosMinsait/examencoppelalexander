//
//  ServiceManager.swift
//  AlexExamenCoppel
//
//  Created by Alexander on 28/01/22.
//

import Foundation


class APIRequest {
    
    static let shared = APIRequest()
    var api_key = "12e0eb93808be66e1f4adde952ff6444"
    
    public init() {
        
    }
    
    func logOutSession(sessionId: String, closure: @escaping(Result<LoginResponse, Error>) -> Void){
        //https://api.themoviedb.org/3/authentication/session?api_key=<<api_key>>
        
        let session = URLSession.shared
        let url = URL(string: "https://api.themoviedb.org/3/authentication/session?api_key=\(api_key)&language=en-US")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        
        let params = [
            "session_id": sessionId
        ] as [String:Any]
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        
        let task = session.dataTask(with: request) { data, response, error in
            
            if let data = data {
                do {
                    
                    let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                    print(json!)
                    let decoder = JSONDecoder()
                    let res = try decoder.decode(LoginResponse.self, from: data)
                    closure(.success(res))
                    print(res)
                } catch {
                    closure(.failure(error))
                    print("JSON error: \(error.localizedDescription)")
                }
            }
        }
        
        task.resume()
    }
    
    func createSession(params: LoginRequest, closure: @escaping(Result<LoginResponse, Error>) -> Void){
        //https://api.themoviedb.org/3/authentication/session/new?api_key=<<api_key>>

        let session = URLSession.shared
        let url = URL(string: "https://api.themoviedb.org/3/authentication/session/new?api_key=\(api_key)&language=en-US")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let params = [
            "request_token": params.request_token
        ] as [String:Any]
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        
        let task = session.dataTask(with: request) { data, response, error in
            
            if let data = data {
                do {
                    
                    let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                    print(json!)
                    let decoder = JSONDecoder()
                    let res = try decoder.decode(LoginResponse.self, from: data)
                    closure(.success(res))
                    print(res)
                } catch {
                    closure(.failure(error))
                    print("JSON error: \(error.localizedDescription)")
                }
            }
        }
        
        task.resume()
    }
    
    func retrieveDetailMovies(idMovie: Int, closure: @escaping(Result<Results, Error>) -> Void){
        //https://api.themoviedb.org/3/movie/634649?api_key=12e0eb93808be66e1f4adde952ff6444&language=en-US
        
        let session = URLSession.shared
        let url = URL(string: "https://api.themoviedb.org/3/movie/\(idMovie)?api_key=\(api_key)&language=en-US")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request) { data, response, error in
            
            if let data = data {
                do {
                    
                    let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                    print(json!)
                    let decoder = JSONDecoder()
                    let res = try decoder.decode(Results.self, from: data)
                    closure(.success(res))
                    print(res)
                } catch {
                    closure(.failure(error))
                    print("JSON error: \(error.localizedDescription)")
                }
            }
        }
        
        task.resume()
    }
    
    func retrieveSession(params: LoginRequest, closure: @escaping(Result<LoginResponse, Error>) -> Void){
        
        let session = URLSession.shared
        let url = URL(string: "https://api.themoviedb.org/3/authentication/token/validate_with_login?api_key=\(api_key)")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let params = [
            "username": params.username,
            "password": params.password,
            "request_token": params.request_token
            
        ] as [String:Any]
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        
        let task = session.dataTask(with: request) { data, response, error in
            
            if let data = data {
                do {
                    
                    let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                    print(json!)
                    let decoder = JSONDecoder()
                    let res = try decoder.decode(LoginResponse.self, from: data)
                    closure(.success(res))
                    print(res)
                } catch {
                    closure(.failure(error))
                    print("JSON error: \(error.localizedDescription)")
                }
            }
        }
        
        task.resume()
    }
    
    func getToken(closure: @escaping(Result<TokenResponse, Error>) -> Void ){
        
        let session = URLSession.shared
        let url = URL(string: "https://api.themoviedb.org/3/authentication/token/new?api_key=\(api_key)")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: url) { data, response, error in
            
            if let data = data {
                do {
                    
                    let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                    print(json!)
                    let decoder = JSONDecoder()
                    let res = try decoder.decode(TokenResponse.self, from: data)
                    closure(.success(res))
                    print(res)
                } catch {
                    closure(.failure(error))
                    print("JSON error: \(error.localizedDescription)")
                }
            }
        }
        
        task.resume()
    }
    
    func retireveListMovies(request: ListMovieRequest ,closure: @escaping(Result<ListMovieResponse, Error>) -> Void){
        
        let session = URLSession.shared
        let url = URL(string: "https://api.themoviedb.org/3/movie/popular?api_key=\(api_key)&language=en-US&page=\(request.page ?? 1)")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: url) { data, response, error in
            
            if let data = data {
                do {
                    let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                    //print(json!)
                    let decoder = JSONDecoder()
                    
                    let res = try decoder.decode(ListMovieResponse.self, from: data)
                    closure(.success(res))
                } catch {
                    closure(.failure(error))
                    print("JSON error: \(error.localizedDescription)")
                }
            }
        }
        
        task.resume()
    }
    
    
    func retrieveImage(){
        
        //let url = "https://image.tmdb.org/t/p/w300/"
        
        let session = URLSession.shared
        let url = URL(string: "https://image.tmdb.org/t/p/w300/eG0oOQVsniPAuecPzDD1B1gnYWy.jpg")
        
        
        
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: url!) { data, response, error in
            
            if let data = data {
                do {
                    let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                    print(json!)
                    let decoder = JSONDecoder()
                    
                    //let res = try decoder.decode(ListMovieResponse.self, from: data)
                    //closure(.success(res))
                    //print(res)
                } catch {
                    //closure(.failure(error))
                    print("JSON error: \(error.localizedDescription)")
                }
            }
        }
        
        task.resume()
    }
    
}
