//
//  LoginAccountLoginAccountConfiguratorTests.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import XCTest

class LoginAccountModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = LoginAccountViewControllerMock()
        let configurator = LoginAccountModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "LoginAccountViewController is nil after configuration")
        XCTAssertTrue(viewController.output is LoginAccountPresenter, "output is not LoginAccountPresenter")

        let presenter: LoginAccountPresenter = viewController.output as! LoginAccountPresenter
        XCTAssertNotNil(presenter.view, "view in LoginAccountPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in LoginAccountPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is LoginAccountRouter, "router is not LoginAccountRouter")

        let interactor: LoginAccountInteractor = presenter.interactor as! LoginAccountInteractor
        XCTAssertNotNil(interactor.output, "output in LoginAccountInteractor is nil after configuration")
    }

    class LoginAccountViewControllerMock: LoginAccountViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
