//
//  ListPelisListPelisConfiguratorTests.swift
//  AlexViperTest
//
//  Created by alexander on 05/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import XCTest

class ListPelisModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = ListPelisViewControllerMock()
        let configurator = ListPelisModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "ListPelisViewController is nil after configuration")
        XCTAssertTrue(viewController.output is ListPelisPresenter, "output is not ListPelisPresenter")

        let presenter: ListPelisPresenter = viewController.output as! ListPelisPresenter
        XCTAssertNotNil(presenter.view, "view in ListPelisPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in ListPelisPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is ListPelisRouter, "router is not ListPelisRouter")

        let interactor: ListPelisInteractor = presenter.interactor as! ListPelisInteractor
        XCTAssertNotNil(interactor.output, "output in ListPelisInteractor is nil after configuration")
    }

    class ListPelisViewControllerMock: ListPelisViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
