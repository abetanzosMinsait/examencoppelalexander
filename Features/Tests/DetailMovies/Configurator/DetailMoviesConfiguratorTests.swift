//
//  DetailMoviesDetailMoviesConfiguratorTests.swift
//  AlexViperTest
//
//  Created by alexander on 07/02/2022.
//  Copyright © 2022 alx. All rights reserved.
//

import XCTest

class DetailMoviesModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = DetailMoviesViewControllerMock()
        let configurator = DetailMoviesModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "DetailMoviesViewController is nil after configuration")
        XCTAssertTrue(viewController.output is DetailMoviesPresenter, "output is not DetailMoviesPresenter")

        let presenter: DetailMoviesPresenter = viewController.output as! DetailMoviesPresenter
        XCTAssertNotNil(presenter.view, "view in DetailMoviesPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in DetailMoviesPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is DetailMoviesRouter, "router is not DetailMoviesRouter")

        let interactor: DetailMoviesInteractor = presenter.interactor as! DetailMoviesInteractor
        XCTAssertNotNil(interactor.output, "output in DetailMoviesInteractor is nil after configuration")
    }

    class DetailMoviesViewControllerMock: DetailMoviesViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
